package load

type FormattedLogger func(string, ...interface{})
type BasicLogger func(string)

// defaultFormattedLogger is a FormattedLogger that does nothing
var defaultFormattedLogger = func(_ string, _ ...interface{}) {}

// defaultBasicLogger is a BasicLogger that does nothing
var defaultBasicLogger = func(_ string) {}
