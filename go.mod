module gitlab.com/zephinzer/load

go 1.15

require (
	github.com/chromedp/cdproto v0.0.0-20200209033844-7e00b02ea7d2
	github.com/chromedp/chromedp v0.5.3
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
)
