package load

import (
	"context"
	"net/url"
	"sync"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
)

type getEventHandlerOptions struct {
	UseData         *Data
	BasicLogger     BasicLogger
	FormattedLogger FormattedLogger
	UseWaiter       *sync.WaitGroup
}

func getEventHandler(ctx context.Context, options getEventHandlerOptions) func(interface{}) {
	log := options.BasicLogger
	if log == nil {
		log = defaultBasicLogger
	}
	logf := options.FormattedLogger
	if logf == nil {
		logf = defaultFormattedLogger
	}
	data := options.UseData
	if data == nil {
		panic(".UseData is required but not specified in the provided :options")
	}
	waiter := options.UseWaiter
	if waiter == nil {
		panic(".UseWaiter is required but not specified in the provided :options")
	}

	return func(event interface{}) {
		waiter.Add(1)
		go func() {
			defer waiter.Done()
			switch ev := event.(type) {
			case *network.EventLoadingFailed:
				logrus.Warnf("request with id '%s' failed: %s / %s", ev.RequestID, ev.ErrorText, ev.BlockedReason.String())
			case *network.EventRequestWillBeSent:
				for key, value := range ev.Request.Headers {
					data.OutgoingSize += uint64(len(key))
					if val, ok := value.([]byte); ok {
						data.OutgoingSize += uint64(len(val))
					}
				}
				if ev.Request.HasPostData {
					data.OutgoingSize += uint64(len(ev.Request.PostData))
				}
			case *network.EventResponseReceived:
				responseUrl, _ := url.Parse(ev.Response.URL)
				resource := Resource{
					ID:                ev.RequestID.String(),
					URL:               responseUrl,
					StatusCode:        ev.Response.Status,
					ResponseSizeBytes: uint64(ev.Response.EncodedDataLength),
				}
				log("- - - - - request log begin - - - - - \n")
				logf("request url   : '%s'\n", resource.URL)
				logf("response code : '%v'\n", resource.StatusCode)
				logf("response size : '%v bytes'\n", resource.ResponseSizeBytes)
				data.Size += uint64(resource.ResponseSizeBytes)

				// request/response metrics
				if ev.Response.Timing != nil {
					timings := ev.Response.Timing
					logf("response duration: %vms\n", timings.ReceiveHeadersEnd)

					logf("- dns resolution: ")
					if anyFloat64IsUnset(timings.DNSEnd, timings.DNSStart) {
						log("n/a\n")
					} else {
						resource.DNSDuration = time.Duration(timings.DNSEnd-timings.DNSStart) * time.Millisecond
						logf("%vms\n", resource.DNSDuration.Milliseconds())
					}

					logf("- proxy resolution: ")
					if anyFloat64IsUnset(timings.ProxyEnd, timings.ProxyStart) {
						log("n/a\n")
					} else {
						resource.ProxyDuration = time.Duration(timings.ProxyEnd-timings.ProxyStart) * time.Millisecond
						logf("%vms\n", resource.ProxyDuration.Milliseconds())
					}

					logf("- ssl handshake: ")
					if anyFloat64IsUnset(timings.SslEnd, timings.SslStart) {
						log("n/a\n")
					} else {
						resource.SSLDuration = time.Duration(timings.SslEnd-timings.SslStart) * time.Millisecond
						logf("%vms\n", resource.SSLDuration.Milliseconds())
					}

					logf("- connection: ")
					if anyFloat64IsUnset(timings.ConnectEnd, timings.ConnectStart) {
						log("n/a\n")
					} else {
						resource.ConnectDuration = time.Duration(timings.ConnectEnd-timings.ConnectStart) * time.Millisecond
						logf("%vms\n", resource.ConnectDuration.Milliseconds())
					}

					logf("- request sending: ")
					if anyFloat64IsUnset(timings.SendEnd, timings.SendStart) {
						log("n/a\n")
					} else {
						resource.SendDuration = time.Duration(timings.SendEnd-timings.SendStart) * time.Millisecond
						logf("%vms\n", resource.SendDuration.Milliseconds())
					}
				}

				// body metrics
				c := chromedp.FromContext(ctx)
				rb := network.GetResponseBody(ev.RequestID)
				if body, err := rb.Do(cdp.WithExecutor(ctx, c.Target)); err != nil {
					logf("error getting body: %s\n", err)
				} else {
					resource.Body = body
					resource.BodySizeBytes = uint64(len(string(body)))
					logf("body size: %v bytes\n", resource.BodySizeBytes)
					data.Size += resource.BodySizeBytes
				}
				setResourceType(&data.ResourceCount, ev.Type)
				data.Resources = append(data.Resources, resource)
				log("- - - - - request log end - - - - - \n")
			}
		}()
	}
}

func setResourceType(resource *ResourceCount, resourceType network.ResourceType) {
	switch resourceType {
	case network.ResourceTypeDocument:
		resource.Documents++
	case network.ResourceTypeStylesheet:
		resource.Stylesheets++
	case network.ResourceTypeImage:
		resource.Images++
	case network.ResourceTypeMedia:
		resource.Medias++
	case network.ResourceTypeFont:
		resource.Fonts++
	case network.ResourceTypeScript:
		resource.Scripts++
	case network.ResourceTypeTextTrack:
		resource.TextTracks++
	case network.ResourceTypeXHR:
		resource.XHRs++
	case network.ResourceTypeFetch:
		resource.Fetches++
	case network.ResourceTypeEventSource:
		resource.EventSources++
	case network.ResourceTypeWebSocket:
		resource.WebSockets++
	case network.ResourceTypeManifest:
		resource.Manifests++
	case network.ResourceTypeSignedExchange:
		resource.SignedExchanges++
	case network.ResourceTypePing:
		resource.Pings++
	case network.ResourceTypeCSPViolationReport:
		resource.CSPViolationReports++
	case network.ResourceTypeOther:
		resource.Others++
	}
	resource.Total++
}
