package load

const (
	UNSET_FLOAT_64 = -1
)

// anyFloat64IsUnset checks if any of the provided float64 vals are unset,
// with unset being defined as the value of UNSET_FLOAT_64
func anyFloat64IsUnset(vals ...float64) bool {
	result := false
	for i := 0; i < len(vals); i++ {
		result = result || (vals[i] == UNSET_FLOAT_64)
	}
	return result
}
