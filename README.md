# Load

Load is a tool that loads a provided website in-memory.

Intended use-cases:

1. Measure network bandwidth required for various popular sites
2. Measure network performance of various popular sites

# Usage

> You'll need both `docker` and `go` installed to build and run this respectively

Run `make deps` to get the binaries

Run `make build` to build the binary at `./bin/load`

Run `./bin/load $URL` to load a website at `$URL`

Run `./bin/load -o json $URL` to get output in json

# Acks

- This is made possible by [github.com/chromedp/chromdep](https://github.com/chromedp/chromdep)

# Development Notes

1. The version of `github.com/chromedp/cdproto` that ships with the main `chromedp` package is not working, resulting in an error like `ERROR: could not unmarshal event: parse error: expected string near offset XXX of 'headers'`, to fix this, the `cdproto` package had to be updated manually to use `github.com/chromedp/cdproto v0.0.0-20200209033844-7e00b02ea7d2` (see [#598](https://github.com/chromedp/chromedp/issues/598) for details)

# License

MIT.
