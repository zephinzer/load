package load

import (
	"net/url"
	"time"
)

// Data represents a data structure for storing a single loaded webpage
type Data struct {
	Time          time.Duration `json:"time"`
	Size          uint64        `json:"size"`
	OutgoingSize  uint64        `json:"outgoingSize"`
	Resources     Resources     `json:"resources"`
	ResourceCount ResourceCount `json:"resourceCount"`
}

type ResourceCount struct {
	Total,
	Documents,
	Stylesheets,
	Images,
	Medias,
	Fonts,
	Scripts,
	TextTracks,
	XHRs,
	Fetches,
	EventSources,
	WebSockets,
	Manifests,
	SignedExchanges,
	Pings,
	CSPViolationReports,
	Others uint64
}

// Resources is a slice of Resource instances
type Resources []Resource

// Resource represents a logical resource that is loaded from the webpage
type Resource struct {
	// metadata
	// ID is the request ID and it should be unique when in a set
	ID string `json:"id"`
	// URL is the URL that returned this Resource instance
	URL *url.URL `json:"url"`
	// Body contains the response body (if applicable)
	Body []byte `json:"body"`
	// sizes
	BodySizeBytes     uint64 `json:"bodySizeBytes"`
	ResponseSizeBytes uint64 `json:"responseSizeBytes"`
	// durations
	ConnectDuration time.Duration `json:"connectDuration"`
	DNSDuration     time.Duration `json:"dnsDuration"`
	ProxyDuration   time.Duration `json:"proxyDuration"`
	SendDuration    time.Duration `json:"sendDuration"`
	SSLDuration     time.Duration `json:"sslDuration"`
	StatusCode      int64         `json:"statusCode"`
}
