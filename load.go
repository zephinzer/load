package load

import (
	"context"
	"sync"
	"time"

	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
)

// LoadOptions provides options for the .Load method
type LoadOptions struct {
	BasicLogger     BasicLogger
	FormattedLogger FormattedLogger
}

// Load loads the webpage located at the provided :address and returns
// a Data instance containing metrics of the webpage
func Load(address string, options LoadOptions) Data {
	var data Data
	var waiter sync.WaitGroup
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()
	ctx, cancel = context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	chromedp.ListenTarget(ctx,
		getEventHandler(ctx, getEventHandlerOptions{
			BasicLogger:     options.BasicLogger,
			FormattedLogger: options.FormattedLogger,
			UseData:         &data,
			UseWaiter:       &waiter,
		}))

	if err := chromedp.Run(ctx, getPageLoader(address)); err != nil {
		panic(err)
	}
	waiter.Wait()
	return data
}

// getPageLoader returns a chromedp task list to be executed
func getPageLoader(address string) chromedp.Tasks {
	return chromedp.Tasks{
		network.Enable(),
		chromedp.Navigate(address),
	}
}
