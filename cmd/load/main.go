package main

import (
	"fmt"
	"net/url"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/load"
)

var globalWaiter sync.WaitGroup
var totalNetworkLoad uint64

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	GetCommand().Execute()
}

func GetCommand() *cobra.Command {
	return &cobra.Command{
		Use: "load <website url>",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("requires a url argument")
			}
			if _, err := url.Parse(args[0]); err != nil {
				return fmt.Errorf("requires a valid url argument: %s", err)
			}
			return nil
		},
		Short: "loads a webpage and displays metrics on it's loading",
		Run: func(cmd *cobra.Command, args []string) {
			parsedUrl, err := url.Parse(args[0])
			if err != nil {
				logrus.Errorf("failed to parse provided <website url> '%s'", args[0])
				return
			}
			if !parsedUrl.IsAbs() {
				logrus.Errorf("provided url '%s' is not absolute", args[0])
				return
			}
			results := load.Load(parsedUrl.String(), load.LoadOptions{})
			for _, resource := range results.Resources {
				logrus.Infof("%s | %s", resource.ID, resource.URL.String())
			}
			fmt.Printf("total requests made       : %v\n", len(results.Resources))
			fmt.Printf("total resources retrieved : %v\n", results.ResourceCount.Total)
			fmt.Printf("  documents             : %v\n", results.ResourceCount.Documents)
			fmt.Printf("  stylesheets           : %v\n", results.ResourceCount.Stylesheets)
			fmt.Printf("  images                : %v\n", results.ResourceCount.Images)
			fmt.Printf("  medias                : %v\n", results.ResourceCount.Medias)
			fmt.Printf("  fonts                 : %v\n", results.ResourceCount.Fonts)
			fmt.Printf("  scripts               : %v\n", results.ResourceCount.Scripts)
			fmt.Printf("  text tracks           : %v\n", results.ResourceCount.TextTracks)
			fmt.Printf("  xhrs                  : %v\n", results.ResourceCount.XHRs)
			fmt.Printf("  fetches               : %v\n", results.ResourceCount.Fetches)
			fmt.Printf("  event sources         : %v\n", results.ResourceCount.EventSources)
			fmt.Printf("  web sockets           : %v\n", results.ResourceCount.WebSockets)
			fmt.Printf("  manifests             : %v\n", results.ResourceCount.Manifests)
			fmt.Printf("  signed exchanges      : %v\n", results.ResourceCount.SignedExchanges)
			fmt.Printf("  pings                 : %v\n", results.ResourceCount.Pings)
			fmt.Printf("  csp violation reports : %v\n", results.ResourceCount.CSPViolationReports)
			fmt.Printf("  others                : %v\n", results.ResourceCount.Others)
			fmt.Printf("total outgoing load        : %v bytes\n", results.OutgoingSize)
			fmt.Printf("total outgoing load        : %v kilobytes\n", results.OutgoingSize/1024)
			fmt.Printf("total outgoing load        : %v megabytes\n", results.OutgoingSize/1024/1024)
			fmt.Printf("total incoming load        : %v bytes\n", results.Size)
			fmt.Printf("total incoming load        : %v kilobytes\n", results.Size/1024)
			fmt.Printf("total incoming load        : %v megabytes\n", results.Size/1024/1024)
			fmt.Printf("total transferred          : %v\n", results.Size+results.OutgoingSize)
		},
	}
}
